package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

}
