package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public final class NullComparatorException extends AbstractException {

    public NullComparatorException() {
        super("Error! Comparator is null...");
    }

}
