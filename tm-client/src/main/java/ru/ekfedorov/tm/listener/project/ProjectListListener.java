package ru.ekfedorov.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractProjectListener;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectListListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable List<Project> projects;
        if (isEmpty(sort)) projects = projectEndpoint.findProjectAll(session);
        else projects = projectEndpoint.findProjectAllWithComparator(session, sort);
        if (projects.isEmpty()) {
            System.out.println("--- There are no projects ---");
            return;
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + toString(project));
            index++;
        }
        System.out.println();
    }

    @NotNull
    public String toString(@NotNull final Project project) {
        return project.getId() + ": " + project.getName() + " | " + project.getStatus();
    }

}
