package ru.ekfedorov.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractTaskListener;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByNameListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Change task status by name.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "change-task-status-by-name";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskChangeStatusByNameListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = taskEndpoint.findTaskOneByName(session, name);
        if (task == null) throw new NullTaskException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        taskEndpoint.changeTaskStatusByName(session, name, status);
    }

}
