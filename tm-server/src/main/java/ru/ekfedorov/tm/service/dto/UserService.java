package ru.ekfedorov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.api.repository.dto.IUserRepository;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.dto.IUserService;
import ru.ekfedorov.tm.dto.User;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.LoginExistException;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    public IUserRepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserRepository getRepository() {
        return userRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final User user) {
        if (user == null) throw new NullObjectException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.add(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<User> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final IUserRepository userRepository = getRepository();
        entities.forEach(userRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findOneById(id).isPresent();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<User> findAll() {
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<User> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        @NotNull final IUserRepository userRepository = getRepository();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<User> comparator = sortType.getComparator();
        return userRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<User> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final IUserRepository userRepository = getRepository();
            userRepository.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login, @Nullable final String password
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (isEmpty(email)) throw new EmailIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @SneakyThrows
    @NotNull
    @Override
    @Transactional
    public Optional<User> findByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findByLogin(login);
    }

    @SneakyThrows
    @Override
    @Transactional
    public boolean isLoginExist(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findByLogin(login).isPresent();
    }

    @SneakyThrows
    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLocked(true);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final User entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.removeOneById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.removeByLogin(login);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) return;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.get().setPasswordHash(hash);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.update(user.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLocked(false);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.update(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) throw new NullObjectException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.update(user.get());
    }

}
