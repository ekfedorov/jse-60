package ru.ekfedorov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.api.repository.dto.ITaskRepository;
import ru.ekfedorov.tm.api.service.dto.ITaskService;
import ru.ekfedorov.tm.dto.Task;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    public ITaskRepository taskRepository;

    @NotNull
    public ITaskRepository getRepository() {
        return taskRepository;
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.add(task);
        return task;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.clearByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @SneakyThrows
    @NotNull
    @Override
    public Optional<Task> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<Task> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<Task> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Task entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.removeOneById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final Task entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        @NotNull Optional<Task> task = taskRepository.findOneByIndex(userId, index);
        if (!task.isPresent()) throw new UserNotFoundException();
        taskRepository.removeOneByIdAndUserId(userId, task.get().getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.removeOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Task task) {
        if (task == null) throw new NullObjectException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Task> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        entities.forEach(taskRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.clear();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskRepository.findOneById(id).isPresent();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final ITaskRepository taskRepository = getRepository();
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Task> comparator = sortType.getComparator();
        return taskRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findOneById(
            @Nullable final String id
    ) {
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskRepository.findOneById(id);

    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Task> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    public @NotNull List<Task> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Task> comparator = sortType.getComparator();
        return taskRepository
                .findAllByUserId(userId)
                .stream().sorted(comparator)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Task> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<Task> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

}
