package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.api.repository.model.IProjectGraphRepository;
import ru.ekfedorov.tm.api.repository.model.IUserGraphRepository;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import ru.ekfedorov.tm.model.ProjectGraph;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

@Service
public final class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectGraphService {

    @NotNull
    @Autowired
    public IProjectGraphRepository projectGraphRepository;

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @NotNull
    public IProjectGraphRepository getRepository() {
        return projectGraphRepository;
    }

    @NotNull
    public IUserGraphRepository getUserRepository() {
        return userGraphRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.add(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        entities.forEach(projectRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.clear();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.findOneById(id).isPresent();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<ProjectGraph> comparator = sortType.getComparator();
        return projectRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<ProjectGraph> findOneById(
            @Nullable final String id
    ) {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneById(id);
    }

    @SneakyThrows
    @Override
    public ProjectGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        project.setUser(userRepository.findOneById(userId).get());
        projectRepository.begin();
        projectRepository.add(project);
        return project;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
            projectRepository.clearByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<ProjectGraph> findAll(@Nullable final String userId) {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        return projectRepository.findAllByUserId(userId);
    }


    @SneakyThrows
    @NotNull
    @Override
    public Optional<ProjectGraph> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<ProjectGraph> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<ProjectGraph> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final ProjectGraph entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        @NotNull Optional<ProjectGraph> project = projectRepository.findOneByIndex(userId, index);
        if (!project.isPresent()) throw new UserNotFoundException();
        projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    public @NotNull List<ProjectGraph> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<ProjectGraph> comparator = sortType.getComparator();
        return projectRepository
                .findAllByUserId(userId)
                .stream().sorted(comparator)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

}
