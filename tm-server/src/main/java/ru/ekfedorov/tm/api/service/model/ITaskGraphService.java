package ru.ekfedorov.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.TaskGraph;

import java.util.List;
import java.util.Optional;

public interface ITaskGraphService extends IGraphBusinessService<TaskGraph> {

    @NotNull
    TaskGraph add(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @SneakyThrows
    void changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @SneakyThrows
    void clear(@Nullable String userId);

    @NotNull
    @SneakyThrows
    List<TaskGraph> findAll(@Nullable String userId);

    @SneakyThrows
    @NotNull Optional<TaskGraph> findOneById(
            @Nullable String userId, @Nullable String id
    );

    @NotNull
    @SneakyThrows
    Optional<TaskGraph> findOneByIndex(
            @Nullable String userId, @Nullable Integer index
    );

    @NotNull
    @SneakyThrows
    Optional<TaskGraph> findOneByName(
            @Nullable String userId, @Nullable String name
    );

    @SneakyThrows
    void remove(@Nullable TaskGraph entity);

    @SneakyThrows
    void remove(
            @Nullable String userId, @Nullable TaskGraph entity
    );

    @SneakyThrows
    void removeOneById(
            @Nullable String userId, @Nullable String id
    );

    @SneakyThrows
    void removeOneByIndex(
            @Nullable String userId, @Nullable Integer index
    );

    @SneakyThrows
    void removeOneByName(
            @Nullable String userId, @Nullable String name
    );

    @SneakyThrows
    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}
