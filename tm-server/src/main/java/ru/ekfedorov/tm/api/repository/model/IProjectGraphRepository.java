package ru.ekfedorov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IGraphRepository;
import ru.ekfedorov.tm.model.ProjectGraph;

import java.util.List;
import java.util.Optional;

public interface IProjectGraphRepository extends IGraphRepository<ProjectGraph> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<ProjectGraph> findAll();

    @NotNull
    List<ProjectGraph> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<ProjectGraph> findOneById(@Nullable String id);

    @NotNull
    Optional<ProjectGraph> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Optional<ProjectGraph> findOneByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    Optional<ProjectGraph> findOneByName(
            @Nullable String userId, @NotNull String name
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}
