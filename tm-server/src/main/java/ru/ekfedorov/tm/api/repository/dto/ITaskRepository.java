package ru.ekfedorov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ekfedorov.tm.api.repository.IRepository;
import ru.ekfedorov.tm.dto.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IRepository<Task> {

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<Task> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<Task> findOneById(@Nullable String id);

    @NotNull
    Optional<Task> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Optional<Task> findOneByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    @NotNull
    Optional<Task> findOneByName(
            @Nullable String userId, @NotNull String name
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

    void unbindTaskFromProjectId(@NotNull String userId, @NotNull String id);

}