package ru.ekfedorov.tm.exception;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractException extends Exception {

    public AbstractException(@Nullable final String message) {
        super(message);
    }

}
