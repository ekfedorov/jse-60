package ru.ekfedorov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.ekfedorov.tm.api.repository.dto.IUserRepository;
import ru.ekfedorov.tm.dto.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public void clear() {
        entityManager
                .createQuery("DELETE FROM User e")
                .executeUpdate();
    }

    @NotNull
    public List<User> findAll() {
        return entityManager
                .createQuery("SELECT e FROM User e", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @NotNull Optional<User> findByLogin(@Nullable final String login) {
        return getSingleResult(entityManager
                .createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1));
    }

    @Override
    public @NotNull Optional<User> findOneById(@Nullable final String id) {
        return Optional.ofNullable(entityManager.find(User.class, id));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM User e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    public void removeOneById(@Nullable final String id) {
        User reference = entityManager.getReference(User.class, id);
        entityManager.remove(reference);
    }

}
