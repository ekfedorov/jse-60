package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.cofiguration.ServerConfiguration;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.service.TestUtil;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class UserGraphServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IUserGraphService userService = context.getBean(IUserGraphService.class);

    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        context.getBean(EntityManagerFactory.class).createEntityManager();
    }

    @After
    public void after() {
        context.getBean(EntityManagerFactory.class).close();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<UserGraph> users = new ArrayList<>();
        final UserGraph user1 = new UserGraph();
        final UserGraph user2 = new UserGraph();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertTrue(userService.findOneById(user1.getId()).isPresent());
        Assert.assertTrue(userService.findOneById(user2.getId()).isPresent());
        userService.remove(users.get(0));
        userService.remove(users.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final UserGraph user = new UserGraph();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final UserGraph user = new UserGraph();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertTrue(userService.contains(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int userSize = userService.findAll().size();
        userService.create("testFindAll", "test", "-");
        Assert.assertEquals(userSize + 1, userService.findAll().size());
        userService.removeByLogin("testFindAll");
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        final UserGraph user = new UserGraph();
        user.setLogin("testFindL");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.findByLogin(login).isPresent());
        userService.removeByLogin("testFindL");
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final UserGraph user = new UserGraph();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final UserGraph user = new UserGraph();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.findOneById(userId).isPresent());
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void isLoginExist() {
        final UserGraph user = new UserGraph();
        user.setLogin("testExist");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.isLoginExist(login));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        final UserGraph user = new UserGraph();
        user.setLogin("testRemoveByLogin");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.removeByLogin(login);
        Assert.assertFalse(userService.isLoginExist(login));
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final UserGraph user = new UserGraph();
        userService.add(user);
        final String userId = user.getId();
        userService.removeOneById(userId);
        Assert.assertFalse(userService.findOneById(userId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final UserGraph user = new UserGraph();
        userService.add(user);
        userService.remove(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
    }

}
